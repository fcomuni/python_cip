import unittest
import datetime
from cip import CIP


class CipTest(unittest.TestCase):
    cip = None
    def setUp(self):
        if self.cip is None:
            self.cip = CIP("http://cumulus.local:8080/CIP/", "localhost")
            self.cip.login('cumulus', 'admin', 'Sample Catalog')

    def test_query(self):
        r = self.cip.do("metadata/search/Sample Catalog", data={
                     'querystring': '"ID" is 271',
                     'table':'AssetRecords',
                     'field': ["ID", "Asset Name", "Asset Modification Date"],
            })

        self.assertTrue( 'items' in r )
        self.assertEqual( len(r['items']), 1 )
        self.assertTrue( 'ID' in r['items'][0] )



if __name__ == '__main__':
    unittest.main()
